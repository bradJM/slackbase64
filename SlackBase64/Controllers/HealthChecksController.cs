using Microsoft.AspNetCore.Mvc;

namespace SlackBase64.Controllers
{
    /// <summary>
    /// Provides health check endpoints for monitoring.
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    [Route("HealthChecks")]
    public class HealthChecksController : Controller
    {
        /// <summary>
        /// A basic health check requiring no external services.
        /// </summary>
        /// <returns>An empty OK (200) response</returns>
        [HttpGet]
        [Route("Basic")]
        public IActionResult Basic()
        {
            return Ok();
        }
    }
}
