using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using SlackBase64.Commands;
using SlackBase64.Configuration;
using SlackBase64.Encoders;

namespace SlackBase64.Controllers
{
    /// <summary>
    /// Handles commands sent from Slack.
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    [Route("api/base64")]
    public class Base64Controller : Controller
    {
        private readonly SlackConfiguration _slackConfiguration;

        /// <summary>
        /// Initializes a new instance of the <see cref="Base64Controller"/> class.
        /// </summary>
        /// <param name="slackConfiguration">The Slack configuration.</param>
        public Base64Controller(IOptions<SlackConfiguration> slackConfiguration)
        {
            _slackConfiguration = slackConfiguration.Value;
        }

        /// <summary>
        /// Handles a Slack command. Dispatches to an encode or decode helper, or returns help text, based on the
        /// command's text content.
        /// </summary>
        /// <param name="slackCommand">The Slack command.</param>
        /// <returns>
        /// Encoded, decoded, or help text in an OK (200) response, an unauthorized (401) response on a missing or
        /// invalid authentication token, or a bad request (400) response on invalid command text.
        /// </returns>
        [HttpPost]
        [Route("")]
        public IActionResult Handler([FromForm] SlackCommand slackCommand)
        {
            if (string.IsNullOrEmpty(slackCommand.Token) || slackCommand.Token != _slackConfiguration.Token)
            {
                return Unauthorized();
            }

            string[] parts = slackCommand.Text?.Split(default(string[]), 2, StringSplitOptions.RemoveEmptyEntries);
            string command = null;
            string text = null;

            if (parts != null && parts.Length >= 1)
            {
                command = parts[0];
            }

            if (parts != null && parts.Length == 2)
            {
                text = parts[1];
            }

            switch (command)
            {
                case "encode":
                    return Encode(text);
                case "decode":
                    return Decode(text);
                case "help":
                    return Ok("Usage: [encode|decode] [text]");
                default:
                    return BadRequest("Please include a command. Valid commands are: encode, decode, help.");
            }
        }

        /// <summary>
        /// Encodes a string using base64.
        /// </summary>
        /// <param name="plainText">The string.</param>
        /// <returns>Encoded text in an OK (200) response, or a bad request (400) response on null input.</returns>
        private IActionResult Encode(string plainText)
        {
            if (plainText == null)
            {
                return BadRequest("Please include text to encode, or use help for usage details.");
            }

            string encodedText = Base64Encoder.Encode(plainText);

            return Ok($"Encoded: {encodedText}");
        }

        /// <summary>
        /// Decodes a base64-encoded string.
        /// </summary>
        /// <param name="base64Text">The string.</param>
        /// <returns>
        /// Decoded text in an OK (200) response, or a bad request (400) response on null or invalid input.
        /// </returns>
        private IActionResult Decode(string base64Text)
        {
            if (base64Text == null)
            {
                return BadRequest("Please include text to decode, or use help for usage details.");
            }

            try
            {
                string decoded = Base64Encoder.Decode(base64Text);

                return Ok($"Decoded: {decoded}");
            }
            catch (FormatException)
            {
                return BadRequest("Sorry, I wasn't able to decode that. Please check the format of your text and try again.");
            }
        }
    }
}
