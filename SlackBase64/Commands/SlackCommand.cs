using System;

namespace SlackBase64.Commands
{
    /// <summary>
    /// Wrapper class for the form data sent by Slack.
    /// </summary>
    public class SlackCommand
    {
        public string Token { get; set; }

        public string Team_Id { get; set; }

        public string Team_Domain { get; set; }

        public string Channel_Id { get; set; }

        public string Channel_Name { get; set; }

        public string User_Id { get; set; }

        public string User_Name { get; set; }

        public string Command { get; set; }

        public string Text { get; set; }

        public Uri Response_Url { get; set; }
    }
}
