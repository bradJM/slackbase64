using System;
using System.Text;

namespace SlackBase64.Encoders
{
    /// <summary>
    /// Provides static helper methods for working with base64 encoding.
    /// </summary>
    public static class Base64Encoder
    {
        /// <summary>
        /// Encodes a string using base64.
        /// </summary>
        /// <param name="data">The string.</param>
        /// <returns></returns>
        public static string Encode(string data)
        {
            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            return Convert.ToBase64String(Encoding.UTF8.GetBytes(data));
        }

        /// <summary>
        /// Decodes a base64-encoded string.
        /// </summary>
        /// <param name="data">The string.</param>
        /// <returns></returns>
        public static string Decode(string data)
        {
            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            return Encoding.UTF8.GetString(Convert.FromBase64String(data));
        }
    }
}
