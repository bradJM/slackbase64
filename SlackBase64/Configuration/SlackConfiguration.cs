namespace SlackBase64.Configuration
{
    /// <summary>
    /// Configuration class for Slack-related options.
    /// </summary>
    public class SlackConfiguration
    {
        /// <summary>
        /// The authentication token sent by Slack. Used to verify the source of requests.
        /// </summary>
        public string Token { get; set; }
    }
}
