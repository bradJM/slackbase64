using System;
using SlackBase64.Encoders;
using Xunit;

namespace SlackBase64.Tests.Encoders
{
    public class Base64EncoderTests
    {
        [Fact]
        public void Encode_CorrectlyBase64Encodes_AlphanumericInput()
        {
            const string expected =
                "YWJjZGVmZ2hpamtsbW5vcHFyc3R1dnd4eXpBQkNERUZHSElKS0xNTk9QUVJTVFVWV1hZWjEyMzQ1Njc4OTA=";
            const string data = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            string encoded = Base64Encoder.Encode(data);

            Assert.Equal(expected, encoded);
        }

        [Fact]
        public void Decode_CorrectlyBase64Decodes_EncodedAlphanumericInput()
        {
            const string expected = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            const string data =
                "YWJjZGVmZ2hpamtsbW5vcHFyc3R1dnd4eXpBQkNERUZHSElKS0xNTk9QUVJTVFVWV1hZWjEyMzQ1Njc4OTA=";
            string decoded = Base64Encoder.Decode(data);

            Assert.Equal(expected, decoded);
        }

        [Fact]
        public void Encode_CorrectlyBase64Encodes_PunctuationInput()
        {
            const string expected = "YH4hQCMkJV4mKigpLV89K1t7XX1cfDs6JyIsPC4+Lz8=";
            const string data = @"`~!@#$%^&*()-_=+[{]}\|;:'"",<.>/?";
            string encoded = Base64Encoder.Encode(data);

            Assert.Equal(expected, encoded);
        }

        [Fact]
        public void Decode_CorrectlyBase64Decodes_EncodedPunctuationInput()
        {
            const string expected = @"`~!@#$%^&*()-_=+[{]}\|;:'"",<.>/?";
            const string data = "YH4hQCMkJV4mKigpLV89K1t7XX1cfDs6JyIsPC4+Lz8=";
            string decoded = Base64Encoder.Decode(data);

            Assert.Equal(expected, decoded);
        }

        [Fact]
        public void Encode_CorrectlyBase64Encodes_UnicodeInput()
        {
            const string expected = "442c45W95a2v6JOz6Kud7LCs6JmG4L6j7LCv5I2G6ImJ4oau5amv4ZOq6oq95pCq";
            const string data = "㍜㕽孯蓳諝찬虆ྣ찯䍆艉↮婯ᓪꊽ搪";
            string encoded = Base64Encoder.Encode(data);

            Assert.Equal(expected, encoded);
        }

        [Fact]
        public void Decode_CorrectlyBase64Decodes_EncodedUnicodeInput()
        {
            const string expected = "㍜㕽孯蓳諝찬虆ྣ찯䍆艉↮婯ᓪꊽ搪";
            const string data = "442c45W95a2v6JOz6Kud7LCs6JmG4L6j7LCv5I2G6ImJ4oau5amv4ZOq6oq95pCq";
            string decoded = Base64Encoder.Decode(data);

            Assert.Equal(expected, decoded);
        }

        [Fact]
        public void Encode_ThrowsArgumentNullException_OnNullInput()
        {
            Assert.Throws<ArgumentNullException>(() => Base64Encoder.Encode(null));
        }

        [Fact]
        public void Decode_ThrowsArgumentNullException_OnNullInput()
        {
            Assert.Throws<ArgumentNullException>(() => Base64Encoder.Decode(null));
        }

        [Fact]
        public void Decode_ThrowsFormatException_OnInvalidInput()
        {
            Assert.Throws<FormatException>(() => Base64Encoder.Decode("bad"));
        }
    }
}
