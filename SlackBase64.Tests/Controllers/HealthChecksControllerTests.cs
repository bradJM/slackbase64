using Microsoft.AspNetCore.Mvc;
using SlackBase64.Controllers;
using Xunit;

namespace SlackBase64.Tests.Controllers
{
    public class HealthChecksControllerTests
    {
        [Fact]
        public void BasicAction_ReturnsOkResult()
        {
            var controller = new HealthChecksController();
            var result = controller.Basic();

            Assert.NotNull(result);
            Assert.IsType<OkResult>(result);
        }
    }
}
