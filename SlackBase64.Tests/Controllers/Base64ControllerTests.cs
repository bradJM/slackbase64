using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using SlackBase64.Commands;
using SlackBase64.Configuration;
using SlackBase64.Controllers;
using Xunit;

namespace SlackBase64.Tests.Controllers
{
    public class Base64ControllerTests
    {
        [Fact]
        public void Handler_ReturnsUnauthorized_OnMissingCommandToken()
        {
            var controller = new Base64Controller(Options.Create(new SlackConfiguration { Token = "abcd1234" }));
            var slackCommand = new SlackCommand { Text = "help" };
            var result = controller.Handler(slackCommand);

            Assert.IsType<UnauthorizedResult>(result);
        }

        [Fact]
        public void Handler_ReturnsUnauthorized_OnIncorrectCommandToken()
        {
            var controller = new Base64Controller(Options.Create(new SlackConfiguration { Token = "abcd1234" }));
            var slackCommand = new SlackCommand { Text = "help", Token = "bcde2345" };
            var result = controller.Handler(slackCommand);

            Assert.IsType<UnauthorizedResult>(result);
        }

        [Fact]
        public void Handler_ReturnsHelpText_OnHelpCommand()
        {
            var controller = new Base64Controller(Options.Create(new SlackConfiguration { Token = "abcd1234" }));
            var slackCommand = new SlackCommand { Text = "help", Token = "abcd1234" };
            var result = controller.Handler(slackCommand);

            Assert.IsType<OkObjectResult>(result);
            Assert.Equal("Usage: [encode|decode] [text]", ((OkObjectResult)result).Value.ToString());
        }

        [Fact]
        public void Handler_ReturnsEncodedText_OnValidTextToEncode()
        {
            var controller = new Base64Controller(Options.Create(new SlackConfiguration { Token = "abcd1234" }));
            var slackCommand = new SlackCommand { Text = "encode hello", Token = "abcd1234" };
            var result = controller.Handler(slackCommand);

            Assert.IsType<OkObjectResult>(result);
            Assert.Equal("Encoded: aGVsbG8=", ((OkObjectResult)result).Value.ToString());
        }

        [Fact]
        public void Handler_ReturnsDecodedText_OnValidTextToDecode()
        {
            var controller = new Base64Controller(Options.Create(new SlackConfiguration { Token = "abcd1234" }));
            var slackCommand = new SlackCommand { Text = "decode aGVsbG8=", Token = "abcd1234" };
            var result = controller.Handler(slackCommand);

            Assert.IsType<OkObjectResult>(result);
            Assert.Equal("Decoded: hello", ((OkObjectResult)result).Value.ToString());
        }

        [Fact]
        public void Handler_ReturnsBadRequest_OnEmptyCommandText()
        {
            var controller = new Base64Controller(Options.Create(new SlackConfiguration { Token = "abcd1234" }));
            var result = controller.Handler(new SlackCommand { Token = "abcd1234" });

            Assert.IsType<BadRequestObjectResult>(result);
            Assert.Equal("Please include a command. Valid commands are: encode, decode, help.",
                         ((BadRequestObjectResult)result).Value.ToString());
        }

        [Fact]
        public void Handler_ReturnsBadRequest_OnNullTextToEncode()
        {
            var controller = new Base64Controller(Options.Create(new SlackConfiguration { Token = "abcd1234" }));
            var slackCommand = new SlackCommand { Text = "encode", Token = "abcd1234" };
            var result = controller.Handler(slackCommand);

            Assert.IsType<BadRequestObjectResult>(result);
            Assert.Equal("Please include text to encode, or use help for usage details.",
                         ((BadRequestObjectResult)result).Value.ToString());
        }

        [Fact]
        public void Handler_ReturnsBadRequest_OnNullTextToDecode()
        {
            var controller = new Base64Controller(Options.Create(new SlackConfiguration { Token = "abcd1234" }));
            var slackCommand = new SlackCommand { Text = "decode", Token = "abcd1234" };
            var result = controller.Handler(slackCommand);

            Assert.IsType<BadRequestObjectResult>(result);
            Assert.Equal("Please include text to decode, or use help for usage details.",
                         ((BadRequestObjectResult)result).Value.ToString());
        }

        [Fact]
        public void Handler_ReturnsBadRequest_OnInvalidTextToDecode()
        {
            var controller = new Base64Controller(Options.Create(new SlackConfiguration { Token = "abcd1234" }));
            var slackCommand = new SlackCommand { Text = "decode abcd12345", Token = "abcd1234" };
            var result = controller.Handler(slackCommand);

            Assert.IsType<BadRequestObjectResult>(result);
            Assert.Equal("Sorry, I wasn't able to decode that. Please check the format of your text and try again.",
                         ((BadRequestObjectResult)result).Value.ToString());
        }
    }
}
